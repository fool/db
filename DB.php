<?php

/**
 * Copyright 2013 Jordan Sterling
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

use \Exception;
use \mysqli;

/**
 * Jordan's awesome PHP DB simplification "library"...
 */
class DB
{
	const MYSQL_DATETIME_FORMAT = 'Y-m-d H:i:s';

	/**
	 * @var String
	 */
	private $host;

	/**
	 * @var String
	 */
	private $user;

	/**
	 * @var String
	 */
	private $pass;

	/**
	 * @var String
	 */
	private $db_name;

	/**
	 * Database handle
	 * @var mysqli
	 */
	private $db = null;

	public function __construct($host, $user, $pass, $db_name)
	{
		$this->host = $host;
		$this->user = $user;
		$this->pass = $pass;
		$this->db_name = $db_name;
	}

	/**
	 * Give a prepared statement query and array of params for it.
	 * Params need to be typed correctly.
	 * Returns
	 * associative array of results on success
	 * boolean false on failure to execute
	 *
	 * if this is an insert query, it returns the object id on success
	 *
	 *
	 * throws an exception if it cant even parse your query
	 * @param string query   Your query parameterized with prepared statements
	 * @param array  params  Array of values for your prepared statement
	 * @return array|int     If insert, returns int. otherwise returns array of results
	 */
	public function query($query, $params = array())
	{
		if ($db == null)
		{
			if ($this->db == null)
				$this->connect();
			$db = $this->db;
		}
		if (!$stmt = $db->prepare($query))
		{
			throw new Exception("Unable to prepare query:\n$query\n Error message: {$db->error}");
		}
		$query = trim($query);
		$select_query = preg_match('/^select/i', $query);
		$insert_query = preg_match('/^insert/i', $query);

		// build the string of prepared types
		$typestring = implode('', array_map(array('DB', 'get_type_string'), $params));

		// Bind parameters
		if (strlen($typestring) > 0)
		{
			array_unshift($params, $typestring);
			$refs = array();
			foreach($params as $key => $value)
				$refs[$key] = &$params[$key];
			call_user_func_array(array($stmt, 'bind_param'), $refs);
		}
		// execute
		if (!$stmt->execute())
		{
			throw new Exception("Unable to execute query:\n$query\nError Message: {$db->error}");
		}
		if ($insert_query)
			return $stmt->insert_id;

		$results = array();
		if ($select_query)
		{
			// get the column names the query asked for
			$stmt->store_result();
			$data = $stmt->result_metadata();
			$fields = array();
			$row = array();
			$fields[0] = &$stmt;
			$count = 1;
			// this dynamically creates an array where each key is the name of the field.
			while ($field = $data->fetch_field())
			{
				$fields[$count] = &$row[$field->name];
				$count++;
			}
			call_user_func_array(array($stmt,'bind_result'), $row);
			while ($stmt->fetch())
			{
				// $row is bound by address, so we need to do a deep copy
				$copyOfRow = array();
				foreach ($row as $name => $val)
					$copyOfRow[$name] = $val;
				$results[] = $copyOfRow;
			}
		}
		$stmt->close();
		return $results;
	}

	/**
	 * Checks for variables type
	 */
	public function get_type_string($variable)
	{
		if (is_int($variable))
			return 'i';
		else if (is_float($variable))
			return 'd';
		else if (is_string($variable))
			return 's';
		return 'b';

	}

	private function connect()
	{
		$this->db = new mysqli($this->host, $this->user, $this->pass, $this->db_name);
		if ($this->db->connect_error)
		{
			throw new Exception('db_connection', "Connect Error ({$this->db->connect_errno}) {$this->db->connect_error}");
		}
	}

	public function close()
	{
		if ($this->db != null)
			$this->db->close();
	}
}
